const colors = require('tailwindcss/colors')

module.exports = {
  // purge: [],
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'cream': "#FFFDD1",
        'scarlet': "#77002e",
        rose: colors.rose,
        "facebook": "#4390f4",
      },
    },
  },
  variants: {
    extend: {
    },
  },
  plugins: [],
  // enabled: process.env.NODE_ENV === "production",
}
