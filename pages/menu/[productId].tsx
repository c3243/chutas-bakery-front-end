import { GetStaticPaths, GetStaticProps, GetStaticPropsContext, GetStaticPropsResult, NextPage } from "next";
import { useRouter } from 'next/router';
import { ParsedUrlQuery } from 'querystring'
import Layout from "../../components/layout/Layout";
import Image from 'next/image'
import { PRODUCTS } from "../../const/products";

interface Props {
    productId: string
    products: any
}

const MenuPage: NextPage<Props> = (props) => {
    const router = useRouter();

    // const productId = router.query.productId;
    console.log(props.productId)

    const product = props.products[parseInt(props.productId)]
    console.log(product);


    return <Layout>
        <div className="p-1 flex flex-col flex-grow">
            <div className="max-w-7xl mx-3 py-6 px-4 sm:px-6 lg:px-8">
                <h1 className="text-2xl font-bold text-gray-900">ซาลาเปา</h1>
                {/* <h1 className="text-2xl font-bold text-gray-900">ขนม: {productId}</h1> */}
            </div>
            <div className="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-4">
                <div className="grid grid-rows-2 sm:col-span-1">
                    <div>
                        <div className="sm:h-64 md:h-80 w-auto m-3 relative rounded-lg overflow-hidden">
                            <Image
                                src="/images/bao-4.jpg"
                                alt=""
                                layout="fill"
                                className="object-center object-cover"
                            />
                        </div>
                    </div>
                    <div>
                        <div className="sm:h-64 md:h-80 w-auto m-3 relative rounded-lg overflow-hidden">
                            <Image
                                src="/images/bao-4.jpg"
                                alt=""
                                layout="fill"
                                className="object-center object-cover"
                            />
                        </div>
                    </div>
                </div>
                <div className="m-3">
                    <div className=" md:h-full w-full relative rounded-lg overflow-hidden">
                        <Image
                            src="/images/bao-4.jpg"
                            alt=""
                            layout="fill"
                            className="object-center object-cover"
                        />
                    </div>
                </div>
                <div className="shadow mt-3 rounded-lg lg:col-span-1 md:col-span-2 p-6">
                    <div className="text-2xl font-bold">18 บาท</div>
                    <div className="mt-6 mb-3">รสชาติ</div>
                    <div className="flex flex-grow flex-wrap flex-row gap-4">
                        <button className="text-gray-500 rounded-md border-2 border-gray-200 p-2 hover:bg-rose-200 hover:border-rose-700 hover:text-rose-800">ถั่วทอง</button>
                        <button className="text-gray-500 rounded-md border-2 border-gray-200 p-2 hover:bg-rose-200 hover:border-rose-700 hover:text-rose-800">เผือก</button>
                        <button className="text-gray-500 rounded-md border-2 border-gray-200 p-2 hover:bg-rose-200 hover:border-rose-700 hover:text-rose-800">งาดำ</button>
                        <button className="text-gray-500 rounded-md border-2 border-gray-200 p-2 hover:bg-rose-200 hover:border-rose-700 hover:text-rose-800">มะพร้าว</button>
                    </div>
                </div>
            </div>

        </div>
    </Layout>
};

interface IParams extends ParsedUrlQuery {
    productId: string
}
export const getStaticPaths: GetStaticPaths = async () => {
    const allIds: string[] = ["1", "2", "3"]
    const paths = allIds.map((productId) => {
        return {
            params: { productId },
        }
    })

    return { paths, fallback: true }
}

export const getStaticProps: GetStaticProps = async (context: GetStaticPropsContext) => {
    // Fetch data from API

    // const productId = context?.params?.productId;
    const { productId } = context.params as IParams
    console.log(productId);
    return {
        props: {
            products: PRODUCTS,
            productId: productId
        },
        revalidate: 300 // fetch every 300 sec
    };
}
export default MenuPage
