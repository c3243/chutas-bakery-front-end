import { NextPage } from "next";
import Link from 'next/link';
import Layout from "../../components/layout/Layout";
import ProductItem from "../../components/menu/ProductItem";

const products = [
    {
        id: 1,
        name: "ซิ่วท้อ",
        varient: "ไส้ถั่ว เผือก งา มะพร้าว",
        price: "฿18",
        imageSrc: "/images/bao.jpg",
        href: "/menu/1"
    },
    {
        id: 2,
        name: "ซาลาเปา",
        varient: "ไส้ถั่ว เผือก งา มะพร้าว",
        price: "฿18",
        imageSrc: "/images/bao.jpg",
        href: "/menu/1"
    },
    {
        id: 3,
        name: "คุกกี้",
        varient: "ไส้ถั่ว เผือก งา มะพร้าว",
        price: "฿20",
        imageSrc: "/images/bao.jpg",
        href: "/menu/1"
    },

]

const MenuPage: NextPage = () => {
    return <Layout>
        <div className="bg-white">
            <div className="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
                <h2 className="text-2xl font-extrabold tracking-tight text-gray-900">ขนมของร้านเรา</h2>
                <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                    {/* Product */}
                    {products.map((product) => (
                        <ProductItem
                            id={product.id}
                            key={product.id}
                            imageSrc={product.imageSrc}
                            href={product.href}
                            name={product.name}
                            varient={product.varient}
                            price={product.price}
                        ></ProductItem>
                    ))}
                </div>
            </div>
        </div>
    </Layout>
};

export default MenuPage
