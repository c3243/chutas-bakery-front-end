import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { VoidFunctionComponent } from 'react'
import Layout from '../components/layout/Layout'

const Promo: VoidFunctionComponent<any> = () => {
  return (
    <div className="relative bg-white overflow-hidden">
      <div className="pt-16 pb-80 sm:pt-24 sm:pb-40 lg:pt-40 lg:pb-48">
        <div className="relative max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 sm:static">
          <div className="sm:max-w-lg">
            <h1 className="text-4xl font font-extrabold tracking-tight text-gray-900 sm:text-6xl">
              <div className="mb-1">เบเกอรี่ โฮมเมด</div><div>ที่ดีที่สุด</div>
            </h1>
            <p className="mt-4 text-xl text-gray-500">
              เบเกอรี่เก่าแก่ แต่ขนมอบใหม่ มีขนมตั้งแต่ ซาลาเปา ซิ่วท้อ ฮวกก้วย คุกกี้ เค้ก หรือขนมตามฤดูกาล <br />
              ขนมทุกชิ้นเราใช้วัตถุดิบคุณภาพ อบเหมือนทานเอง
            </p>
          </div>
          <div>
            <div className="mt-10">
              {/* Image */}
              <div
                aria-hidden="true"
                className="pointer-events-none lg:absolute lg:inset-y-0 lg:max-w-7xl lg:mx-auto lg:w-full"
              >
                <div className="absolute transform sm:left-1/2 sm:top-0 sm:translate-x-8 lg:left-1/2 lg:top-1/2 lg:-translate-y-1/2 lg:translate-x-8">
                  <div className="flex items-center space-x-6 lg:space-x-8">
                    <div className="flex-shrink-0 grid grid-cols-1 gap-y-6 lg:gap-y-8">
                      <div className="w-44 h-64 rounded-lg overflow-hidden sm:opacity-0 lg:opacity-100 relative">
                        <Image
                          src="/images/bao-2.jpg"
                          alt=""
                          layout="fill"
                          className="w-full h-full object-center object-cover"
                        />
                      </div>
                      <div className="w-44 h-64 rounded-lg overflow-hidden relative">
                        <Image
                          src="/images/bao.jpg"
                          alt=""
                          layout="fill"
                          className="w-full h-full object-center object-cover"
                        />
                      </div>
                    </div>
                    <div className="flex-shrink-0 grid grid-cols-1 gap-y-6 lg:gap-y-8">
                      <div className="w-44 h-64 rounded-lg overflow-hidden relative">
                        <Image
                          src="/images/bao-4.jpg"
                          alt=""
                          layout="fill"
                          className="w-full h-full object-center object-cover"
                        />
                      </div>
                      <div className="w-44 h-64 rounded-lg overflow-hidden relative">
                        <Image
                          src="/images/bao-3.jpg"
                          alt=""
                          layout="fill"
                          className="w-full h-full object-center object-cover"
                        />
                      </div>
                      <div className="w-44 h-64 rounded-lg overflow-hidden relative">
                        <Image
                          src="/images/cake-2.jpg"
                          alt=""
                          layout="fill"
                          className="w-full h-full object-center object-cover"
                        />
                      </div>
                    </div>
                    <div className="flex-shrink-0 grid grid-cols-1 gap-y-6 lg:gap-y-8">
                      <div className="w-44 h-64 rounded-lg overflow-hidden relative">
                        <Image
                          src="/images/cake-1.jpg"
                          alt=""
                          layout="fill"
                          className="w-full h-full object-center object-cover"
                        />
                      </div>
                      <div className="w-44 h-64 rounded-lg overflow-hidden relative">
                        <Image
                          src="/images/mooncake.jpg"
                          alt=""
                          layout="fill"
                          className="w-full h-full object-center object-cover"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <a
                href="#"
                className="inline-block text-center bg-rose-700 border border-transparent rounded-md py-3 px-8 font-medium text-white hover:bg-rose-500"
              >
                เราทำขนมอะไรบ้าง?
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const Home: NextPage = (props) => {

  return (
    <Layout>
      {/* <div className={styles.container}> */}
      <div>
        <Head>
          <title>Chuta's Bakery</title>
          <meta name="description" content="ร้านขายเบเกอรี่ เราอบใหม่ทุกวัน ทำขนมตามฤดูกาล ซาลาเปา ซิ่วท้อ ไหว้เจ้า จัดกระเช้าวันเกิด ฮวกก้วย งานแต่ง งานบวช งานมงคล" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {Promo(null)}

        <footer className="items-center flex-row">
          <div className="border-t grid justify-items-center items-center h-12 flex-row">
            <a
              href="https://fb.com/ChutasBakery"
              target="_blank"
              className="flex flex-row justify-items-center items-center"
            >
              <Image src="/icons/find-us-fb.svg" alt="Facebook" width={144} height={30} />
              <div className="text-facebook font-sans text-lg font-medium ">
                Chuta's Bakery
              </div>
            </a>
          </div>
        </footer>

      </div>
    </Layout>
  )
}


export default Home
