
type ProductType = {
    id: number,
    name: string,
    varient: string[],
    size: { [size: string]: string },
    imageSrc: string[],
    href: string
}

export const PRODUCTS: { [id: number]: ProductType } = {
    1: {
        id: 1,
        name: "ซิ่วท้อ",
        varient: ["ไส้ถั่ว", "เผือก", "งา", "มะพร้าว"],
        size: { "ปกติ": "18" },
        imageSrc: ["/images/bao.jpg", "/images/bao.jpg", "/images/bao.jpg"],
        href: "/menu/1"
    },
    2: {
        id: 2,
        name: "ซาลาเปา",
        varient: ["ไส้ถั่ว", "เผือก", "งา", "มะพร้าว"],
        size: { "ปกติ": "18" },
        imageSrc: ["/images/bao.jpg", "/images/bao.jpg", "/images/bao.jpg"],
        href: "/menu/2"
    },
    3: {
        id: 3,
        name: "คุกกี้",
        varient: ["ไส้ถั่ว", "เผือก", "งา", "มะพร้าว"],
        size: { "1 ชิ้น": "20" },
        imageSrc: ["/images/bao.jpg", "/images/bao.jpg", "/images/bao.jpg"],
        href: "/menu/2"
    },
}