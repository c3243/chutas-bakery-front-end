import React, { FunctionComponent } from "react";

type CustomButtonProps = {
    title: string
    onClick?: Function
    type?: string
}

const CustomButton: FunctionComponent<CustomButtonProps> = (props: CustomButtonProps) => {
    // return <button
    //     type="button"
    //     className="border border-gray-300 text-gray-700 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-gray-300 focus:outline-none focus:shadow-outline"
    // >
    //     {props.title}
    // </button>
    return <button
        type="button"
        className="border border-gray-300 text-gray-300 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-gray-300 hover:text-gray-700 focus:outline-none focus:shadow-outline"
    >
        Light
    </button>
}

export default CustomButton;