import classes from './Card.module.css';
import React, { FunctionComponent, ReactNode } from 'react';

type CardProps = {
  children: ReactNode
}

const Card: FunctionComponent<CardProps> = ({ children }: CardProps) => {
  return <div className={classes.card}>{children}</div>;
}

export default Card;
