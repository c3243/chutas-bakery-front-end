import MainNavigation from './MainNavigation';
import React, { ReactNode, FunctionComponent } from 'react'
import classes from './Layout.module.css';

type LayoutProps = {
  children?: ReactNode
}

const Layout: FunctionComponent<LayoutProps> = ({ children }: LayoutProps) => {
  return (
    <div>
      <MainNavigation />
      <main>{children}</main>
    </div>
  );
}

export default Layout;
