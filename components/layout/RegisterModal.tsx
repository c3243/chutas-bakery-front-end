import React, { FunctionComponent, useState, Fragment } from 'react';
import { Dialog, Transition } from '@headlessui/react'

type ModalProps = {
    isShow: boolean;
    closeModal: () => void;
}

interface RegisterFormData {
    firstName: string;
    lastName: string;
    email: string;
}


const RegisterModal: FunctionComponent<ModalProps> = ({ isShow, closeModal }: ModalProps) => {
    // let [isOpen, setIsOpen] = useState(true)
    const [registerFormData, setRegisterFormData] = useState<RegisterFormData>({
        firstName: "",
        lastName: "",
        email: ""
    })

    const handleChange = (field: string, event: React.FormEvent<HTMLInputElement>) => {
        // console.log(field, event.currentTarget.value);
        const newRegisterFormData = registerFormData
        switch (field) {
            case "firstName":
                newRegisterFormData.firstName = event.currentTarget.value;
            case "lastName":
                newRegisterFormData.lastName = event.currentTarget.value;
            case "email":
                newRegisterFormData.email = event.currentTarget.value;
        }

        setRegisterFormData(newRegisterFormData)
    }

    // function closeModal() {
    //     setIsOpen(false)
    // }

    // function openModal() {
    //     setIsOpen(true)
    // }

    return (<>
        {/* <div className="fixed inset-0 flex items-center justify-center">
            <button
                type="button"
                onClick={openModal}
                className="px-4 py-2 text-sm font-medium text-white bg-black rounded-md bg-opacity-20 hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
            >
                Open dialog
            </button>
        </div> */}

        <Transition appear show={isShow} as={Fragment}>
            <Dialog
                as="div"
                className="fixed inset-0 z-10 overflow-y-auto"
                onClose={closeModal}
            >

                <Dialog.Overlay className="fixed inset-0 bg-black opacity-30" />
                <div className="min-h-screen px-4 text-center">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span
                        className="inline-block h-screen align-middle"
                        aria-hidden="true"
                    >
                        &#8203;
                    </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 scale-95"
                        enterTo="opacity-100 scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 scale-100"
                        leaveTo="opacity-0 scale-95"
                    >
                        <div className="inline-block w-full max-w-lg p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                            <Dialog.Title
                                as="h3"
                                className="text-lg font-medium leading-6 text-gray-900"
                            >
                                สมัครสมาชิก
                            </Dialog.Title>
                            <div className="mt-2">
                                <p className="text-sm text-gray-500">
                                    เข้าร่วมกับชุตาเบเกอรี่ เพื่อรับข่าวสารและโปรโมชั่นพิเศษ
                                </p>
                                <div className="m-2 space-y-2 mt-4">
                                    <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                        First name
                                    </label>
                                    <input
                                        placeholder="ชื่อจริง"
                                        onChange={(e) => handleChange("firstName", e)}
                                        type="text"
                                        name="first-name"
                                        id="first-name"
                                        autoComplete="given-name"
                                        className="m-1 focus:outline-none focus:ring focus:border-blue-200 block w-full shadow-sm sm:text-sm border-gray-100 border rounded-md p-2"
                                    />
                                    <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                        Last name
                                    </label>
                                    <input
                                        type="text"
                                        name="last-name"
                                        onChange={(e) => handleChange("lastName", e)}
                                        placeholder="นามสกุล"
                                        id="last-name"
                                        autoComplete="family-name"
                                        className="m-1 focus:outline-none focus:ring focus:border-blue-200 block w-full shadow-sm sm:text-sm border-gray-100 border rounded-md p-2"
                                    />
                                    <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                                        Email address
                                    </label>
                                    <input
                                        type="text"
                                        name="email-address"
                                        id="email-address"
                                        onChange={(e) => handleChange("email", e)}
                                        placeholder="อีเมล"
                                        autoComplete="email"
                                        className="m-1 focus:outline-none focus:ring focus:border-blue-200 block w-full shadow-sm sm:text-sm border-gray-100 border rounded-md p-2"
                                    />
                                </div>
                            </div>

                            <div className="mt-4 flex flex-row justify-between">
                                <button
                                    type="button"
                                    className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-gray-300 border border-transparent rounded-md hover:bg-gray-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                                    onClick={closeModal}
                                >
                                    ย้อนกลับ
                                </button>
                                <button
                                    type="button"
                                    className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-rose-700 border border-transparent rounded-md hover:bg-rose-500 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                                    onClick={closeModal}
                                >
                                    สมัครสมาชิก
                                </button>
                            </div>
                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition>
    </>);
}

export default RegisterModal;