import React, { FunctionComponent } from "react";
import Image from 'next/image';
import Link from 'next/link';

type ProductItemProps = {
    id: number
    name: string
    varient: string
    imageSrc: string
    imageAlt?: string
    href: string
    price: string
}

const ProductItem: FunctionComponent<ProductItemProps> = (product: ProductItemProps) => {
    return <div key={product.id} className="group relative">
        <div className="relative w-full min-h-80 bg-gray-200 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
            <Image
                src={product.imageSrc}
                alt={product.imageAlt}
                layout='fill'
                className="w-full h-full object-center object-cover lg:w-full lg:h-full"
            />
        </div>
        <div className="mt-4 flex justify-between">
            <div>
                <h3 className="text-sm text-gray-700">
                    <Link href={product.href}>
                        <a>
                            <span aria-hidden="true" className="absolute inset-0" />
                            <span className="font-medium">{product.name}</span>
                        </a>
                    </Link>
                </h3>
                <p className="mt-1 text-sm text-gray-400">{product.varient}</p>
            </div>
            <p className="text-sm font-medium text-gray-700">{product.price}</p>
        </div>
    </div>;
}

export default ProductItem;