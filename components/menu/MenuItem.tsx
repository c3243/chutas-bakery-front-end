import Card from '../ui/Card';
import classes from './MenuItem.module.css';
import React, { FunctionComponent } from 'react';

type MenuItemProps = {
  id: number
  image: string
  title: string
  description: string
}

const MenuItem: FunctionComponent<MenuItemProps> = (props: MenuItemProps) => {
  return (
    <li className={classes.item}>
      <Card>
        <div className={classes.image}>
          <img src={props.image} alt={props.title} />
        </div>
        <div className={classes.content}>
          <h3>{props.title}</h3>
          <address>{props.description}</address>
        </div>
        <div className={classes.actions}>
          <button>Show Details</button>
        </div>
      </Card>
    </li>
  );
}

export default MenuItem;
