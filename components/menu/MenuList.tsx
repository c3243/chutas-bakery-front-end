import MenuItem from './MenuItem';
import classes from './MenuList.module.css';
import React, { FunctionComponent } from 'react';

type Menu = {
  productId: number
  image: string
  title: string
  description: string
}
type MenuListProps = {
  menus: Menu[]
}


const MeetupList: FunctionComponent<MenuListProps> = (props: MenuListProps) => {
  return (
    <ul className={classes.list}>
      {props.menus.map((menu) => (
        <MenuItem
          key={menu.productId}
          id={menu.productId}
          image={menu.image}
          title={menu.title}
          description={menu.description}
        />
      ))}
    </ul>
  );
}

export default MeetupList;
